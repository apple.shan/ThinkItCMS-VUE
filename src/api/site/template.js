import { axios } from '@/utils/request'
import {servers} from '@/api/servers'
const serverName=servers.cms;
const api = {
  page: serverName+'/template/page',
  save: serverName+'/template/save',
	getById:serverName+'/template/getByPk',
	update: serverName+'/template/update',
	delByPk:serverName+'/template/deleteByPk',
  loadTemplateTree:serverName+'/template/loadTemplateTree',
  getFileContent:serverName+'/template/getFileContent',
  setFileContent:serverName+'/template/setFileContent',
  createFile:serverName+'/template/createFile',
  delFile:serverName+'/template/delFile',
  openFile: serverName+'/template/openFile',
  checkerIsStartTemplate: serverName+'/template/checkerIsStartTemplate',
  uploadFile:serverName+'/template/uploadFile',
  importFile:serverName+'/template/importFile',
  unZipFile: serverName+'/template/unZipFile',
  exportit: serverName+'/template/exportit',
  getDirects: serverName+'/template/getDirects',
  loadPartFile: serverName+'/template/loadPartFile',
  downFiles: serverName+'/template/downFiles'
}
export default api

export function page (parameter) {
  return axios({
    url: api.page,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

export function save (parameter) {
  return axios({
    url: api.save,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

export function update (parameter) {
  return axios({
    url: api.update,
    method: 'put',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

export function uploadFile (parameter) {
  return axios({
    url: api.uploadFile,
    method: 'post',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    },
    data: parameter
  })
}

export function importFile (parameter) {
  return axios({
    url: api.importFile,
    method: 'post',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    },
    data: parameter
  })
}

export function unZipFile (parameter) {
  return axios({
    url: api.unZipFile,
    method: 'post',
    params: parameter
  })
}


export function getById (parameter) {
  return axios({
    url: api.getById,
    method: 'get',
    params: parameter
  })
}

export function getDirects (parameter) {
  return axios({
    url: api.getDirects,
    method: 'get',
    params: parameter
  })
}

export function loadPartFiles (parameter) {
  return axios({
    url: api.loadPartFile,
    method: 'get',
    params: parameter
  })
}




export function checkerIsStartTemplate (parameter) {
  return axios({
    url: api.checkerIsStartTemplate,
    method: 'get',
    params: parameter
  })
}

export function getFileContent (parameter) {
  return axios({
    url: api.getFileContent,
    method: 'get',
    params: parameter
  })
}

export function createFile (parameter) {
  return axios({
    url: api.createFile,
    method: 'put',
    params: parameter
  })
}


export function setFileContent (parameter) {
  return axios({
    url: api.setFileContent,
    method: 'post',
    data: parameter
  })
}

export function loadTempTree (parameter) {
  return axios({
    url: api.loadTemplateTree,
    method: 'get',
    params: parameter
  })
}

export function openFile (parameter) {
  return axios({
    url: api.openFile,
    method: 'get',
    params: parameter
  })
}



export function delByPk(parameter) {
  return axios({
    url: api.delByPk,
    method: 'delete',
    params: parameter
  })
}


export function delFile(parameter) {
  return axios({
    url: api.delFile,
    method: 'delete',
    params: parameter
  })
}



export function exportit (parameter) {
  return axios({
    url: api.exportit,
    method: 'post',
    responseType: 'blob', 
    params: parameter
  })
}

export function downFiles (parameter) {
  return axios({
    url: api.downFiles,
    method: 'post',
    responseType: 'blob', 
    params: parameter
  })
}
