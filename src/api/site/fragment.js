import { axios } from '@/utils/request'
import {servers} from '@/api/servers'
const serverName=servers.cms;
const api = {
    page: serverName+'/fragmentModel/page',
    save: serverName+'/fragmentModel/createFile',
    getById:serverName+'/fragmentModel/get',
    update: serverName+'/fragmentModel/update',
    delByPk:serverName+'/fragmentModel/delete',
    getFragmentInfo:serverName+'/fragmentModel/getFragmentInfo',
    getFragmentFrom: serverName+'/fragmentModel/getFragmentFrom',
    updateFragment: serverName+'/fragmentModel/updateFragment',
    checkerIsStartTemplate:'/fragmentModel/checkerIsStartTemplate',
    getPartImport : serverName+'/fragmentModel/getPartImport',
    //-------------------------------插入数据-----------------------------
    insertFragmentAttr: serverName+'/fragmentAttr/insertFragmentAttr',
    pageFragmentAttr: serverName+'/fragmentAttr/page',
    delFragmentAttr: serverName+'/fragmentAttr/delFragmentAttr',
    getDesignAttrById: serverName+'/fragmentAttr/getDesignAttrById',
    updateFragmentAttr: serverName+'/fragmentAttr/updateFragmentAttr'
    
}
export default api

export function page (parameter) {
  return axios({
    url: api.page,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

export function pageFragmentAttr (parameter) {
  return axios({
    url: api.pageFragmentAttr,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}



export function save (parameter) {
  return axios({
    url: api.save,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

export function insertFragmentAttr (parameter) {
  return axios({
    url: api.insertFragmentAttr,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

export function updateFragmentAttr (parameter) {
  return axios({
    url: api.updateFragmentAttr,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}



export function updateFragment (parameter) {
  return axios({
    url: api.updateFragment,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}



export function update (parameter) {
  return axios({
    url: api.update,
    method: 'put',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

export function getById (parameter) {
  return axios({
    url: api.getById,
    method: 'get',
    params: parameter
  })
}

export function checkerIsStartTemplate (parameter) {
  return axios({
    url: api.checkerIsStartTemplate,
    method: 'get',
    params: parameter
  })
}

export function getPartImport (parameter) {
  return axios({
    url: api.getPartImport,
    method: 'get',
    params: parameter
  })
}


export function getDesignAttrById (parameter) {
  return axios({
    url: api.getDesignAttrById,
    method: 'get',
    params: parameter
  })
}



export function getFragmentInfo (parameter) {
  return axios({
    url: api.getFragmentInfo,
    method: 'get',
    params: parameter
  })
}

export function getFragmentFrom (parameter) {
  return axios({
    url: api.getFragmentFrom,
    method: 'get',
    params: parameter
  })
}


export function delByPk(parameter) {
  return axios({
    url: api.delByPk,
    method: 'delete',
    params: parameter
  })
}

export function delFragmentAttr(parameter) {
  return axios({
    url: api.delFragmentAttr,
    method: 'delete',
    params: parameter
  })
}



