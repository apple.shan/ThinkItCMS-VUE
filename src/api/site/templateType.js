import {
  axios
} from '@/utils/request'
import {
  servers
} from '@/api/servers'
const serverName = servers.cms;
const api = {
  page: serverName + '/templateType/selectTreeList',
  save: serverName + '/templateType/save',
  getById: serverName + '/templateType/getByPk',
  update: serverName + '/templateType/update',
  delByPk: serverName + '/templateType/deleteByPk',
  getTempByPidTypes:serverName+'/templateType/getTempByPidTypes'
}
export default api

export function page(parameter) {
  return axios({
    url: api.page,
    method: 'post',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    },
    data: parameter
  })
}

export function save(parameter) {
  return axios({
    url: api.save,
    method: 'post',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    },
    data: parameter
  })
}

export function update(parameter) {
  return axios({
    url: api.update,
    method: 'put',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    },
    data: parameter
  })
}

export function getById(parameter) {
  return axios({
    url: api.getById,
    method: 'get',
    params: parameter
  })
}

export function getTempByPidType(parameter) {
  return axios({
    url: api.getTempByPidTypes,
    method: 'get',
    params: parameter
  })
}


export function delByPk(parameter) {
  return axios({
    url: api.delByPk,
    method: 'delete',
    params: parameter
  })
}
