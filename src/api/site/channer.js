import { axios } from '@/utils/request'
import {servers} from '@/api/servers'
const serverName=servers.cms;
const api = {
	notifyHomePage:serverName+'/channelNotify/notifyHomePage',
  notifyWholeSite: serverName+'/channelNotify/notifyWholeSite',
  notifyToNosql: serverName+'/channelNotify/notifyDbToNoSql',
  flushdb: serverName+'/channelNotify/flushdb'
}
export default api


export function notifyHomePage (parameter) {
  return axios({
    url: api.notifyHomePage,
    method: 'put',
    params: parameter
  })
}

export function notifyWholeSite (parameter) {
  return axios({
    url: api.notifyWholeSite,
    method: 'put',
    params: parameter
  })
}

export function notifyToNosql (parameter) {
  return axios({
    url: api.notifyToNosql,
    method: 'put',
    params: parameter
  })
}

export function flushdb (parameter) {
  return axios({
    url: api.flushdb,
    method: 'put',
    params: parameter
  })
}






