import { axios } from '@/utils/request'
import {servers} from '@/api/servers'
const serverName=servers.cms;
const api = {
  page: serverName+'/license/page',
  importLicense: serverName+'/license/importLicense'
}
export default api

export function page (parameter) {
  return axios({
    url: api.page,
    method: 'get',
		// headers: {
		//   'Content-Type': 'application/json;charset=UTF-8'
		// },
    params: parameter
  })
}

export function importLicense (parameter) {
  return axios({
    url: api.save,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

