import { axios } from '@/utils/request'
import {servers} from '@/api/servers'
const serverName=servers.cms;
const api = {
  page: serverName+'/category/page',
  treeCategory: serverName+'/category/treeCategory',
  save: serverName+'/category/save',
	getById:serverName+'/category/getDetail',
	update: serverName+'/category/update',
	delByPk:serverName+'/category/deleteByPk',
  loadTemplate:serverName+'/category/loadTemplate',
  loadPathRule: serverName+'/category/loadPathRule',
  
  saveModelBatch: serverName+'/categoryModelRelation/saveBatch',
  saveCategoryAttr: serverName+'/categoryAttr/saveSeo',
  updateCategoryAttr: serverName+'/categoryAttr/updateSeo',
  getSeo: serverName+'/categoryAttr/getSeo',
  genCategory: serverName+'/category/genCategory',
  saveStrategy:'/strategy/saveStrategy',
  queryStrategy:'/strategy/queryStrategy',
  clearStrategy:'/strategy/clearStrategy'
}
export default api

export function treeCategory (parameter) {
  return axios({
    url: api.treeCategory,
    method: 'get',
    params: parameter
  })
}

export function page (parameter) {
  return axios({
    url: api.page,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}


export function loadTemplate(parameter) {
  return axios({
    url: api.loadTemplate,
    method: 'get',
    params: parameter
  })
}


export function loadPathRule(parameter) {
  return axios({
    url: api.loadPathRule,
    method: 'get',
    params: parameter
  })
}




export function save (parameter) {
  return axios({
    url: api.save,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

export function saveCategoryAttrSeo (parameter) {
  return axios({
    url: api.saveCategoryAttr,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

export function updateCategoryAttrSeo (parameter) {
  return axios({
    url: api.updateCategoryAttr,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}



export function update (parameter) {
  return axios({
    url: api.update,
    method: 'put',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

export function getById (parameter) {
  return axios({
    url: api.getById,
    method: 'get',
    params: parameter
  })
}


export function delByPk(parameter) {
  return axios({
    url: api.delByPk,
    method: 'delete',
    params: parameter
  })
}



export function saveModelBatch (parameter) {
  return axios({
    url: api.saveModelBatch,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}


export function getSeo(parameter) {
  return axios({
    url: api.getSeo,
    method: 'get',
    params: parameter
  })
}

export function genCategory(parameter) {
  return axios({
    url: api.genCategory,
    method: 'put',
    params: parameter
  })
}


export function saveStrategy (parameter) {
  return axios({
    url: api.saveStrategy,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

export function queryStrategy(parameter) {
  return axios({
    url: api.queryStrategy,
    method: 'get',
    params: parameter
  })
}



export function clearStrategy(parameter) {
  return axios({
    url: api.clearStrategy,
    method: 'delete',
    params: parameter
  })
}
