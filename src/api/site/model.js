/* eslint-disable no-mixed-spaces-and-tabs */
import { axios } from '@/utils/request'
import {servers} from '@/api/servers'
const serverName=servers.cms;
const api = {
  page: serverName + '/model/page',
  save: serverName + '/model/save',
  getById: serverName + '/model/getByPk',
  update: serverName + '/model/update',
  delByPk: serverName + '/model/delete',
  loadModel: serverName + '/model/loadModel',
  loadTemplate:serverName + '/model/loadTemplate',
  listModel: serverName + '/model/listModel',
  listPublihModel:  serverName + '/model/listPublihModel'
}
export default api

export function page(parameter) {
  return axios({
    url: api.page,
    method: 'post',
    headers: { 'Content-Type': 'application/json;charset=UTF-8' },
    data: parameter
  })
}

export function loadModel(parameter) {
  return axios({
    url: api.loadModel,
    method: 'get',
    params: parameter
  })
}

export function listModel(parameter) {
  return axios({
    url: api.listModel,
    method: 'get',
    params: parameter
  })
}


export function loadTemplate(parameter) {
  return axios({
    url: api.loadTemplate,
    method: 'get',
    params: parameter
  })
}

export function listPublihModel(parameter) {
  return axios({
    url: api.listPublihModel,
    method: 'get',
    params: parameter
  })
}


export function save(parameter) {
  return axios({
    url: api.save,
    method: 'post',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    },
    data: parameter
  })
}

export function update(parameter) {
  return axios({
    url: api.update,
    method: 'put',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    },
    data: parameter
  })
}

export function getById(parameter) {
  return axios({
    url: api.getById,
    method: 'get',
    params: parameter
  })
}

export function delByPk(parameter) {
  return axios({
    url: api.delByPk,
    method: 'delete',
    params: parameter
  })
}

export function listDefaultField(parameter) {
  return axios({
    url: api.listDefaultField,
    method: 'get',
    params: parameter
  })
}

