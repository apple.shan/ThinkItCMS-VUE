/* eslint-disable no-mixed-spaces-and-tabs */
import { axios } from '@/utils/request'
import {servers} from '@/api/servers'
const serverName=servers.admin;
const api = {
  page: serverName + '/dict/treeList',
  save: serverName + '/dict/save',
  getById: serverName + '/dict/getByPk',
  update: serverName + '/dict/update',
  delByPk: serverName + '/dict/delByPk',
  listType: serverName + '/dict/listType',
  listByType: serverName + '/dict/listByType'
}
export default api

export function page (parameter) {
  return axios({
    url: api.page,
    method: 'post',
    headers: {
		   'Content-Type': 'application/json;charset=UTF-8'
    },
    data: parameter
  })
}

export function listByType (parameter) {
  return axios({
    url: api.listByType,
    method: 'get',
    params: parameter
  })
}

export function listType (parameter) {
  return axios({
    url: api.listType,
    method: 'post',
    headers: {
		   'Content-Type': 'application/json;charset=UTF-8'
    },
    data: parameter
  })
}

export function save (parameter) {
  return axios({
    url: api.save,
    method: 'post',
    headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
    },
    data: parameter
  })
}

export function update (parameter) {
  return axios({
    url: api.update,
    method: 'put',
    headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
    },
    data: parameter
  })
}

export function getById (parameter) {
  return axios({
    url: api.getById,
    method: 'get',
    params: parameter
  })
}

export function delByPk (parameter) {
  return axios({
    url: api.delByPk,
    method: 'delete',
    params: parameter
  })
}
