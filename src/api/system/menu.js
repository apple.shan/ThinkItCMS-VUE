import { axios } from '@/utils/request'
import {servers} from '@/api/servers'
const serverName=servers.admin;
const api = {
  page: serverName+'/menu/selectTreeList',
  save: serverName+'/menu/save',
	getById:serverName+'/menu/info',
	update: serverName+'/menu/update',
	delByPk:serverName+'/menu/delete',
	selectRoles:serverName+'/menu/selectAllRoles',
	batch:serverName+'/menu/batch',
  // loadApp:serverName+'/menu/loadApplications',
  loadApp:serverName+'/dict/listByType',
  hideIt:serverName+'/menu/hideIt'
  
}
export default api

export function page (parameter) {
  return axios({
    url: api.page,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

export function save (parameter) {
  return axios({
    url: api.save,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

export function update (parameter) {
  return axios({
    url: api.update,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

export function batch (parameter) {
  return axios({
    url: api.batch,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}



export function getById (parameter) {
  return axios({
    url: api.getById,
    method: 'get',
    params: parameter
  })
}

export function loadApp (parameter) {
  return axios({
    url: api.loadApp,
    method: 'get',
    params: parameter
  })
}


export function hideIt(parameter) {
  return axios({
    url: api.hideIt,
    method: 'put',
    params: parameter
  })
}


export function delByPk(parameter) {
  return axios({
    url: api.delByPk,
    method: 'delete',
    params: parameter
  })
}

export function selectRoles (parameter) {
  return axios({
    url: api.selectRoles,
    method: 'get',
    params: parameter
  })
}

export function getPermissions (parameter) {
  return axios({
    url: api.permissionNoPager,
    method: 'get',
    params: parameter
  })
}

