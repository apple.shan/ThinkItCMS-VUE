import { axios } from '@/utils/request'
import {servers} from '@/api/servers'
const serverName=servers.admin;
const api = {
  page: serverName+'/clients/page',
  save: serverName+'/clients/save',
	getById:serverName+'/clients/getByPk',
	update: serverName+'/clients/update',
  delByPk:serverName+'/clients/delete',
  queryAssignResource:serverName+'/apiSource/selectTreeSourceByCli',
	saveAssignResource:serverName+'/apiClient/assignResource',
}
export default api

export function page (parameter) {
  return axios({
    url: api.page,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

export function listClient (parameter) {
  return axios({
    url: api.listClient,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

export function save (parameter) {
  return axios({
    url: api.save,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

export function update (parameter) {
  return axios({
    url: api.update,
    method: 'put',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}

export function getById (parameter) {
  return axios({
    url: api.getById,
    method: 'get',
    params: parameter
  })
}


export function delByPk(parameter) {
  return axios({
    url: api.delByPk,
    method: 'delete',
    params: parameter
  })
}


export function queryAssignResource(parameter) {
  return axios({
    url: api.queryAssignResource,
    method: 'get',
    params: parameter
  })
}

export function saveAssignResource(parameter) {
  return axios({
    url: api.saveAssignResource,
    method: 'post',
		headers: {
		  'Content-Type': 'application/json;charset=UTF-8'
		},
    data: parameter
  })
}


