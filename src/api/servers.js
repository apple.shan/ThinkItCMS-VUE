
// 微服务名称
const servers = {
  auth: '',
  admin:'',
  cms:'',
  member: '',
  product:''
}

const api = {
  Login: servers.auth+'/oauth/token',
  Logout: servers.auth+'/auth/logout',
  license: '/api/license/search',
  ForgePassword: '/forge-password',
  Register: '/register',
  twoStepCode: '/2step-code',
  SendSms: '/account/sms',
  SendSmsErr: '/account/sms_err',
  UserInfo: servers.admin+'/user/info'
}

export default api
export {
  servers
}