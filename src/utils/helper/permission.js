export const PERMISSION_ENUM = {
  'add': { key: 'add', label: '新增' },
  'delete': { key: 'delete', label: '删除' },
  'edit': { key: 'edit', label: '修改' },
  'query': { key: 'query', label: '查询' },
  'get': { key: 'get', label: '详情' },
  'enable': { key: 'enable', label: '启用' },
  'disable': { key: 'disable', label: '禁用' },
  'import': { key: 'import', label: '导入' },
  'export': { key: 'export', label: '导出' }
}

function plugin (Vue) {
  if (plugin.installed) {
    return
  }

  !Vue.prototype.$auth && Object.defineProperties(Vue.prototype, {
    $auth: {
      get () {
        const _this = this
        return (permissions) => {
           let contains = true
           const permissionList = _this.$store.getters.roles
           if(permissions && permissionList.length>0){
              if(permissions.indexOf('|')===-1){
                  contains= permissionList.includes(permissions)
              }else{
                 let permissionArrs = permissions.split('|')
                 if(permissionArrs.length>0){
                   for(let i=0;i<permissionArrs.length;i++){
                     if(!permissionList.includes(permissionArrs[i].replace(/\s+/g,""))){
                       contains = false
                       break
                     }
                   }
                 }
              }
           }else{
             contains = false
           }
           return contains;
        }
      }
    }
  })

  !Vue.prototype.$enum && Object.defineProperties(Vue.prototype, {
    $enum: {
      get () {
        // const _this = this;
        return (val) => {
          let result = PERMISSION_ENUM
          val && val.split('.').forEach(v => {
            result = result && result[v] || null
          })
          return result
        }
      }
    }
  })
  
  !Vue.prototype.$substr && Object.defineProperties(Vue.prototype, {
    $substr: {
      get () {
        return (str,length) => {
          if(str){
             str = str.replace(/(<([^>]+)>)/ig, "").replace(/\s+|\n/g,"");
             let subStr = (str.length > length ? str.substring(0,length)+'..':str)
             return '<span title='+str+'>'+subStr+'</span>'
          }
          return ""
        }
      }
    }
  })
  
  
  
}

export default plugin
