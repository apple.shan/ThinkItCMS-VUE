import storage from 'store'
import { notifyHomePage , notifyWholeSite , notifyToNosql, flushdb } from '@/api/site/channer.js'
import {
  ACCESS_TOKEN,
  DEFAULT_SITE
} from '@/store/mutation-types'


 const _this = this
const notifyIt = {
  mutations: {
  },
  actions: {
    // 通知生成首页
    notifyHome({
      commit
    }, response) {
      return new Promise((resolve, reject) => {
        notifyHomePage().then(response => {
          // if (response.code === 0) {
          //    _this.$message.info('~请稍后~')
          // } else {
          //    _this.$message.info('操作通知异常')
          // }
        })
      })
    },
    notifyWholeSite({
      commit
    }, response) {
      return new Promise((resolve, reject) => {
        notifyWholeSite().then(response => {
          // if (response.code === 0) {
          //    _this.$message.info('~请稍后~')
          // } else {
          //    _this.$message.info('操作通知异常')
          // }
        })
      })
    },
    notifyToNosql({
      commit
    }, response) {
      return new Promise((resolve, reject) => {
        notifyToNosql().then(response => {
          // if (response.code === 0) {
          //    _this.$message.info('~请稍后~')
          // } else {
          //    _this.$message.info('操作通知异常')
          // }
        })
      })
    },
    flushdb({
      commit
    }, response) {
      return new Promise((resolve, reject) => {
        flushdb().then(response => {
          // if (response.code === 0) {
          //    _this.$message.info('~请稍后~')
          // } else {
          //    _this.$message.info('操作通知异常')
          // }
        })
      })
    }
    
    
    
  }
}

export default notifyIt
