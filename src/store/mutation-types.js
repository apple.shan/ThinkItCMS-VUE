export const ACCESS_TOKEN = 'Access-Token'
export const DEFAULT_SITE = 'Default-Site'

export const SIDEBAR_TYPE = 'sidebar_type'
export const TOGGLE_MOBILE_TYPE = 'is_mobile'
export const TOGGLE_NAV_THEME = 'nav_theme'
export const TOGGLE_LAYOUT = 'layout'
export const TOGGLE_FIXED_HEADER = 'fixed_header'
export const TOGGLE_FIXED_SIDEBAR = 'fixed_sidebar'
export const TOGGLE_CONTENT_WIDTH = 'content_width'
export const TOGGLE_HIDE_HEADER = 'auto_hide_header'
export const TOGGLE_COLOR = 'color'
export const TOGGLE_WEAK = 'weak'
export const TOGGLE_MULTI_TAB = 'multi_tab'
export const APP_LANGUAGE = 'app_language'
export const MONIT_PARAMS = 'monit_params'


export const CONTENT_WIDTH_TYPE = {
  Fluid: 'Fluid',
  Fixed: 'Fixed'
}

export const NAV_THEME = {
  LIGHT: 'light',
  DARK: 'dark'
}

export function server(){
  let domain = window.location.host
  let isLocal = (domain.startsWith('10') || domain.startsWith('172') || domain.startsWith('192') || domain.startsWith('127') || domain.startsWith('localhost'))
  if(isLocal){
    return null
  }  
  let str = "s|m|c|t|i|k|n|i|h|t"
  let apiUrl = '/api/registerCenter/registerInfo?domain='+domain;
  let baseUrl= 'http://s.'+str.split('|').reverse().join('')+'.com';
  return baseUrl+apiUrl
}